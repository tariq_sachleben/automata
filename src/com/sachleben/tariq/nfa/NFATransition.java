package com.sachleben.tariq.nfa;

import java.util.Set;

/**
 * Created by tsachleben on 1/12/18.
 */
public interface NFATransition<E, Q> {
  Set<Q> next(Q current, E e) throws IllegalArgumentException;
}
