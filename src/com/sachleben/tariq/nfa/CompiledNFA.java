package com.sachleben.tariq.nfa;

import com.sachleben.tariq.Automata;
import com.sachleben.tariq.Enumerable;
import com.sachleben.tariq.Tuple;
import com.sachleben.tariq.Util;
import com.sachleben.tariq.dfa.DFA;
import com.sachleben.tariq.dfa.DFATransition;

import java.lang.reflect.Array;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeoutException;

/**
 * Created by tsachleben on 1/10/18.
 */
public class CompiledNFA<E extends Enumerable<E>, Q extends Enumerable<Q>> implements Automata<E> {

  private final DFA<E, Set<Q>> compiled;

  public CompiledNFA(NFATransition<E, Q> delta, Q startingState, List<Q> acceptingStates, E example) {
    Set<E> allElements = example.listAll();
    Set<Q> allStates = startingState.listAll();
    Set<Set<Q>> statesPowerSet = Util.powerSet(allStates);
    List<Set<Q>> newAcceptingStates = new ArrayList<Set<Q>>();

    for (Q e : acceptingStates) {
      newAcceptingStates.add(new SingletonSet<Q>(e));
    }

    HashMap<Tuple<E, Set<Q>>, Set<Q>> transitions = new HashMap<Tuple<E, Set<Q>>, Set<Q>>();
    for (Tuple<E, Set<Q>> input : Util.crossProduct(allElements, statesPowerSet)) {
      Set<Q> nextStates = new HashSet<Q>();
      for (Q test : input.b)
        nextStates.addAll(delta.next(test, input.a));
      transitions.put(input, nextStates);
    }
    compiled = new DFA<E, Set<Q>>(new HashMapCompiledTransition<E, Q>(transitions),
            new SingletonSet<Q>(startingState), newAcceptingStates);
  }

  public boolean execute(Iterator<E> s, long timeout) throws TimeoutException, IllegalArgumentException {
    return compiled.execute(s, timeout);
  }

  private static class HashMapCompiledTransition<E, Q> implements DFATransition<E, Set<Q>> {

    private HashMap<Tuple<E, Set<Q>>, Set<Q>> transitions;

    HashMapCompiledTransition(HashMap<Tuple<E, Set<Q>>, Set<Q>> transitions) {
      this.transitions = transitions;
    }

    public Set<Q> next(Set<Q> current, E e) throws IllegalArgumentException {
      return transitions.get(new Tuple<E, Set<Q>>(e, current));
    }
  }

  private static class SingletonSet<T> extends AbstractSet<T> {

    private final T item;

    SingletonSet(T t) {
      item = t;
    }

    public int size() {
      return 1;
    }

    public boolean isEmpty() {
      return false;
    }

    public boolean contains(Object o) {
      return item.equals(o);
    }

    public Iterator<T> iterator() {
      return new Iterator<T>() {

        boolean taken = false;

        public boolean hasNext() {
          return !taken;
        }

        public T next() {
          if (taken)
            throw new NoSuchElementException();
          taken = true;
          return item;
        }

        public void remove() {
          throw new UnsupportedOperationException();
        }
      };
    }

    public Object[] toArray() {
      return new Object[] { item };
    }

    // Take from <http://developer.classpath.org/doc/java/util/ArrayList-source.html>
    public <T1> T1[] toArray(T1[] t1s) {
      if (t1s.length < 1) {
        t1s = (T1[]) Array.newInstance(t1s.getClass().getComponentType(), 1);
      } else if (t1s.length > 1) {
        t1s[1] = null;
      }
      System.arraycopy(toArray(), 0, t1s, 0, 1);
      return t1s;
    }

    public boolean add(T t) {
      throw new UnsupportedOperationException();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException();
    }

    public boolean containsAll(Collection<?> collection) {
      for (Object e : collection)
        if (!e.equals(item))
          return false;
      return true;
    }

    public boolean addAll(Collection<? extends T> collection) {
      throw new UnsupportedOperationException();
    }

    public boolean retainAll(Collection<?> collection) {
      throw new UnsupportedOperationException();
    }

    public boolean removeAll(Collection<?> collection) {
      throw new UnsupportedOperationException();
    }

    public void clear() {
      throw new UnsupportedOperationException();
    }
  }
}
