package com.sachleben.tariq.nfa;

import com.sachleben.tariq.Automata;
import com.sachleben.tariq.Util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeoutException;

/**
 * Implementation of an NFA that executes naively [eg. enumerates all possible states after a
 * transition and evaluates all of them] and naively uses a work queue to manage
 *
 * Note: order of execution generally resembles a breadth-first tree search. Individual execution
 *       orders may vary due to when threads are scheduled by the JVM.
 *
 * Runtime properties
 *  - Size: Omega(1), O(+inf) [consider the case where the transition function produces n > 2
 *          transitions at every step]
 *  - Time: O(2^n) w/ constant but varying factors based on number of worker threads present
 */
public class NaiveQueueNFA<E, Q> implements Automata<E> {

  private final NFATransition<E, Q> delta;
  private final Q startingState;
  private final List<Q> acceptingStates;
  private final int threadCount;

  public NaiveQueueNFA(NFATransition<E, Q> delta, Q startingState, List<Q> acceptingStates, int threads) {
    if (threads < 1)
      throw new IllegalArgumentException("Must have a positive number of threads to spawn");
    this.delta = delta;
    this.startingState = startingState;
    this.acceptingStates = acceptingStates;
    this.threadCount = threads;
  }

  public boolean execute(Iterator<E> s, long timeout) throws TimeoutException, IllegalArgumentException {
    // Check that we'll be doing something interesting, otherwise optimize out
    if (!s.hasNext()) {
      return acceptingStates.contains(startingState);
    }

    // Initialize Local Variables
    ThreadGroup threads = new ThreadGroup("NaiveQueueNFA Execution");
    ProcessStatus status = new ProcessStatus();
    Queue<NFAState> states = new ConcurrentLinkedQueue<NFAState>();

    states.add(new NFAState(startingState, s));

    // Start worker threads
    for (int i = 0; i < this.threadCount; i++)
      new Thread(threads, new ProcessTask(status, states)).start();

    // Run while a solution has not been found, there is work to do, and the timeout has not elapsed
    long startTime = System.currentTimeMillis();
    long timeoutRemaining = timeout;
    while (!status.isFinished() && states.size() > 0 && timeoutRemaining > 0) {
      try {
        long sleepTime = Math.min(1000, timeoutRemaining);
        Thread.sleep(sleepTime);
      } catch (InterruptedException e) {
        System.err.printf("Main Thread Interrupted: \n%s\n", e);
      }
      timeoutRemaining = (startTime + timeout) - System.currentTimeMillis();
    }

    // Clean up threads; give up with clean destruction after 500 ms
    if (!status.isFinished()) status.finished();
    int tries = 0;
    while (threads.activeCount() > 0 && tries < 5)
      try {
        threads.interrupt();
        Thread.sleep(100);
        tries++;
      } catch (InterruptedException e) {
        System.err.printf("Main thread interrupted: \n%s\n", e);
      }
    threads.destroy();

    // Choose exit pattern depending on timeout and results of
    if (timeoutRemaining < 0)
      throw new TimeoutException();
    else if (status.didError())
      throw new IllegalArgumentException(new AggregateException(status.error));
    else
      return status.wasSuccessful();
  }

  // Private Inner record type. Contains a state of the machine and remaining input to be processed.
  private class NFAState {
    final Q state;
    final Iterator<E> s;

    NFAState(Q state, Iterator<E> s) {
      this.state = state;
      this.s = s;
    }
  }

  // Private Inner control type. Manages mutable, parallelized data and process shut-down
  private class ProcessStatus {
    private boolean finished = false;
    private boolean successful = false;
    private List<Exception> error = new ArrayList<Exception>();

    synchronized void finished() { finished = true; }
    synchronized void finishedSuccessful() {
      finished = true;
      successful = true;
    }
    synchronized void errorEncountered(Exception e) {
      finished = true;
      error.add(e);
    }
    boolean isFinished() { return finished; }
    boolean wasSuccessful() { return successful; }
    synchronized boolean didError() { return error.size() > 0; }
  }

  // Private Inner worker type. Core of NFA logic is implemented in void run(void)
  private class ProcessTask implements Runnable {

    private final ProcessStatus status;
    private final Queue<NFAState> queue;

    ProcessTask(ProcessStatus status, Queue<NFAState> queue) {
      this.status = status;
      this.queue = queue;
    }

    public void run() {
      // Handle unexpected errors at the top level so they can be reported
      try {
        // Worker thread will wait ~1.5 seconds without having work before exiting
        int tries = 0;
        while (!status.isFinished() && tries < 15) {
          if (queue.size() == 0) {
            tries++;
            try {
              Thread.sleep(100);
            } catch (InterruptedException e) {
              System.err.println("Worker Interrupted; handled silently.");
            }
          } else {
            // There's currently work in the queue; try to handle as much of it as possible
            tries = 0;
            while (queue.size() > 0) {
              NFAState state = queue.poll();
              Iterator<E> s = state.s;
              Set<Q> newStates = delta.next(state.state, s.next());

              // If there are no more symbols to process, check if one of the new states is accepted
              if (!s.hasNext()) {
                for (Q finish : newStates)
                  if (acceptingStates.contains(finish)) {
                    status.finishedSuccessful();
                    break;
                  }
              } else {
                // Otherwise there are more symbols to process, so schedule all the new states with
                // the remaining symbols for processing
                Collection<E> newS = Util.buildCollection(s);
                for (Q next : newStates) {
                  queue.add(new NFAState(next, newS.iterator()));
                }
              }
            }
          }
        }
      } catch (Exception e) {
        status.errorEncountered(e);
      }
    }
  }

  // Private Inner Exception type to handle encountering multiple Exceptions at once
  // Basics taken from <https://stackoverflow.com/a/2912638>
  private static class AggregateException extends Exception {

    private List<Exception> basket;

    AggregateException(List<Exception> l) {
      if (l.size() == 0)
        throw new IllegalArgumentException();
      else if (l.size() == 1)
        initCause(l.get(0));
      else
        this.basket = l;
    }

    public String getMessage() {
      if (basket == null)
        return super.getCause().getMessage();
      else
        return "Multiple Exceptions Encountered: " + basket.size();
    }
  }
}
