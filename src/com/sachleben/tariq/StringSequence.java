package com.sachleben.tariq;

import java.util.Iterator;

/**
 * Taken from https://stackoverflow.com/a/3925566
 */
public class StringSequence implements Iterator<Character> {

  private final String s;
  private int pos;

  public StringSequence(String s) {
    this.s = s;
    this.pos = 0;
  }

  public boolean hasNext() {
    return pos < s.length();
  }

  public Character next() {
    pos++;
    return s.charAt(pos);
  }

  public void remove() {
    throw new UnsupportedOperationException();
  }
}
