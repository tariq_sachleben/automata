package com.sachleben.tariq;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by tsachleben on 1/13/18.
 */
public class Util {

  public static <T> Set<Set<T>> powerSet(Set<T> source) {
    return null;
  }

  public static <E, F> Collection<Tuple<E, F>> crossProduct(Collection<E> a, Collection<F> b) {
    Collection<Tuple<E, F>> list = new ArrayList<Tuple<E, F>>();
    for (E i : a)
      for (F j : b)
        list.add(new Tuple<E, F>(i, j));
    return list;
  }

  public static <T> Collection<T> buildCollection(Iterator<T> source) {
    ArrayList<T> list = new ArrayList<T>();
    while (source.hasNext()) list.add(source.next());
    return list;
  }
}
