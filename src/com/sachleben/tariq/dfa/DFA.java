package com.sachleben.tariq.dfa;

import com.sachleben.tariq.Automata;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by tsachleben on 1/10/18.
 */
public class DFA<E, Q> implements Automata<E> {

  private final DFATransition<E, Q> delta;
  private final Q startingState;
  private List<Q> acceptingStates;

  public DFA(DFATransition<E, Q> delta, Q startingState, List<Q> acceptingStates) {
    this.delta = delta;
    this.startingState = startingState;
    this.acceptingStates = new ArrayList<Q>(acceptingStates);
  }

  public boolean execute(Iterator<E> s, long timeout) throws IllegalArgumentException {
    Q state = this.startingState;
    while (s.hasNext()) {
      state = this.delta.next(state, s.next());
    }
    return acceptingStates.contains(state);
  }
}
