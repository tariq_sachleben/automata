package com.sachleben.tariq.dfa;

/**
 * Created by tsachleben on 1/10/18.
 */
public interface DFATransition<E, Q> {
  Q next(Q current, E e) throws IllegalArgumentException;
}
