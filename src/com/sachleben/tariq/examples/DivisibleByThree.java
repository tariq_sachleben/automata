package com.sachleben.tariq.examples;

import com.sachleben.tariq.Enumerable;
import com.sachleben.tariq.dfa.DFA;
import com.sachleben.tariq.dfa.DFATransition;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by tsachleben on 1/15/18.
 */
public class DivisibleByThree {

  private enum ModuloThree implements Enumerable<ModuloThree> {
    NULL_INIT, MOD_ZERO, MOD_ONE, MOD_TWO;

    public Set<ModuloThree> listAll() {
      HashSet<ModuloThree> all = new HashSet<ModuloThree>();
      all.add(NULL_INIT);
      all.add(MOD_ZERO);
      all.add(MOD_ONE);
      all.add(MOD_TWO);
      return all;
    }

    int value() {
      switch (this) {
        case NULL_INIT:
        case MOD_ZERO:
          return 0;
        case MOD_ONE:
          return 1;
        case MOD_TWO:
          return 2;
      }
      throw new IllegalArgumentException();
    }

    static ModuloThree forValue(int v) {
      switch (v % 3) {
        case 0: return MOD_ZERO;
        case 1: return MOD_ONE;
        case 2: return MOD_TWO;
      }
      throw new IllegalArgumentException();
    }
  }

  private static class ModuloThreeTransition implements DFATransition<Character, ModuloThree> {

    public ModuloThree next(ModuloThree state, Character c) throws IllegalArgumentException {
      switch (c) {
        case '0':
          return ModuloThree.forValue(state.value() * 2);
        case '1':
          return ModuloThree.forValue((state.value() * 2) + 1);
      }
      throw new IllegalArgumentException();
    }
  }

  public static void main(String[] args) {
    List<ModuloThree> accept = new ArrayList<ModuloThree>();
    accept.add(ModuloThree.MOD_ZERO);
    DFA<Character, ModuloThree> system = new DFA<Character, ModuloThree>(new ModuloThreeTransition(),
            ModuloThree.NULL_INIT, accept);
    new CharacterSystemRunner<ModuloThree>(system).run(System.in, System.out);
  }
}
