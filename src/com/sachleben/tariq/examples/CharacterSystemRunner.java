package com.sachleben.tariq.examples;

import com.sachleben.tariq.dfa.DFA;
import com.sachleben.tariq.types.BinaryString;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Created by tsachleben on 1/15/18.
 */
public class CharacterSystemRunner<Q> {

  private final DFA<Character, Q> system;

  CharacterSystemRunner(DFA<Character, Q> system) {
    this.system = system;
  }

  private static boolean argDoesQuit(String arg) {
    return arg.equals("q") || arg.equals("quit");
  }

  void run(InputStream pIn, PrintStream out) {
    Scanner in = new Scanner(pIn);
    boolean running = true;
    do {
      out.print("> ");
      String arg = in.nextLine();
      if (argDoesQuit(arg)) {
        running = false;
      } else if (!BinaryString.validate(arg)) {
        out.println("Invalid string");
      } else if (system.execute(BinaryString.validatedIterator(arg), 1000)) {
        out.println("Accepts!");
      } else {
        out.println("No Accept!");
      }
    } while (running && in.hasNext());
  }
}
