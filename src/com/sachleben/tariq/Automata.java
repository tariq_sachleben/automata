package com.sachleben.tariq;

import java.util.Iterator;
import java.util.concurrent.TimeoutException;

/**
 * Created by tsachleben on 1/10/18.
 *
 * Immutable enums are strongly encouraged as values for T.
 */
public interface Automata<T> {
  boolean execute(Iterator<T> s, long timeout) throws TimeoutException, IllegalArgumentException; // int timeout in milliseconds
}
