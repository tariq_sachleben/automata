package com.sachleben.tariq;

import java.util.Set;

/**
 * Created by tsachleben on 1/13/18.
 */
public interface Enumerable<T> {
  // This method should be static (it returns the same values regardless of the underlying object
  // being called) and the result should be immutable
  Set<T> listAll();
}
