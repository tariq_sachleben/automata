package com.sachleben.tariq;

/**
 * Created by tsachleben on 1/13/18.
 */
public class Tuple<E, F> {

  public final E a;
  public final F b;

  public Tuple(E a, F b) {
    this.a = a;
    this.b = b;
  }

  private static boolean doEquals(Tuple<?, ?> one, Tuple<?, ?> two) {
    return one.a.equals(two.a) && one.b.equals(two.b);
  }

  // Written with help from <https://stackoverflow.com/a/16386463>
  @Override
  public boolean equals(Object o) {
    return (o instanceof Tuple) && doEquals(this, (Tuple<?, ?>) o);
  }

  // Written with help from
  // <https://stackoverflow.com/questions/2943347/combining-java-hashcodes-into-a-master-hashcode>
  // and <https://www.di-mgt.com.au/primes1000.html>
  @Override
  public int hashCode() {
    return (1699 * a.hashCode()) ^ (599 * b.hashCode());
  }
}
