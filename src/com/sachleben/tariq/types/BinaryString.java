package com.sachleben.tariq.types;


/**
 * Created by tsachleben on 1/15/18.
 */
public class BinaryString {

  private BinaryString() { }

  private static class Iterator implements java.util.Iterator<Character> {

    private final String data;
    private int pos;

    Iterator(String source) {
      if (!validate(source))
        throw new IllegalArgumentException();
      data = source;
      pos = 0;
    }

    public boolean hasNext() {
      return pos < data.length();
    }

    public Character next() {
      char c = data.charAt(pos);
      pos++;
      return c;
    }

    public void remove() {
      throw new UnsupportedOperationException();
    }
  }

  public static java.util.Iterator<Character> validatedIterator(String source)
          throws IllegalArgumentException {
    if (!validate(source))
      throw new IllegalArgumentException();
    return new Iterator(source);
  }

  public static boolean validate(String source) {
    for (int i = 0; i < source.length(); i++) {
      switch (source.charAt(i)) {
        case '0':
        case '1':
          continue;
        default:
          return false;
      }
    }
    return true;
  }
}
